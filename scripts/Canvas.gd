extends Node

var startPosition = Vector2(-100, -100)
var endPosition = Vector2(-100, -100)

onready var brush = get_node("Viewport/brush")

func _ready():
	get_node("Viewport_Sprite").texture = get_node("Viewport").get_texture()
	brush.connect("draw", self, "brush_draw")
	set_process_input(true)

func _input(event):
	if startPosition == Vector2(-100, -100):
		startPosition = event.position
	
	if event.is_pressed():
		startPosition = event.position
		return
	endPosition = event.position
	brush.update()
	
func brush_draw():
	if get_node("/root/global").drawBg:
		# the drawing area = full screen size
		var rect = Rect2(Vector2(0,0),OS.get_screen_size())
		var bgColor = get_node("/root/global").bgColor
		brush.draw_rect(rect,bgColor)
		get_node("/root/global").drawBg = false
		return
	
	var brushColor = get_node("/root/global").brushColor
	var brushSize = get_node("/root/global").brushSize
	
	brush.draw_line(startPosition, endPosition, brushColor, brushSize)
	startPosition = endPosition
	