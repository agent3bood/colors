extends TextureButton

func _ready():
	connect("pressed", self, "on_pressed")
	
func on_pressed():
	# play audio
	get_node("AudioStreamPlayer2D").play()
	# set brush color and size
	get_node("/root/global").brushColor = get_node("color").text
	get_node("/root/global").brushSize = get_node("/root/global").brushSizeDefault
	# hide colors
	get_parent().hide()
	
