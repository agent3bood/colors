extends TextureButton

func _ready():
	connect("pressed", self, "on_press")
	
func on_press():
	var visible = get_parent().get_node("Colors").visible
	get_parent().get_node("Colors").visible = ! visible