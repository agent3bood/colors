extends TextureButton

func _ready():
	connect("pressed", self, "on_press")
	
func on_press():
	get_node("/root/global").brushColor = get_node("/root/global").bgColor
	get_node("/root/global").brushSize = 20